﻿using System.Globalization;
using System.Text.Json;
using System.Web;

namespace WeatherAPI
{
	public class Program
	{
		public static async Task<int> Main(string[] argv)
		{
			string apiKey = "7b6be55ecfc023f52792505653e8e278";
			
			Console.Write("Input your city: ");
			string city = Console.ReadLine();
			
			Console.WriteLine("City: {0} \n", city);
			await PrintTempAndDateByCity(city, apiKey);
			
			return 0;
		}
		
		private static async Task<int> PrintTempAndDateByCoordinate(double lat, double lon, string apiKey)
		{
			var builder = new UriBuilder("https://api.openweathermap.org/data/2.5/onecall");
			var queryParameters = HttpUtility.ParseQueryString(builder.Query);
			
			queryParameters["lat"] = Convert.ToString(lat, CultureInfo.CurrentCulture);
			queryParameters["lon"] = Convert.ToString(lon, CultureInfo.CurrentCulture);
			queryParameters["appid"] = apiKey;
			builder.Query = queryParameters.ToString();
            
			Uri uri = builder.Uri;
			var request = new HttpRequestMessage(HttpMethod.Get, uri);
			var client = new HttpClient();
			
			HttpResponseMessage result = await client.SendAsync(request);
			result.EnsureSuccessStatusCode();

			string jsonContent = await result.Content.ReadAsStringAsync();
			JsonDocument jsonDocument = JsonDocument.Parse(jsonContent);
			
			double kelvinDegrees = jsonDocument.RootElement
				.GetProperty("current")
				.GetProperty("temp")
				.GetDouble();

			int dateUnixTimeSeconds = jsonDocument.RootElement
				.GetProperty("current")
				.GetProperty("dt")
				.GetInt32();

			DateTimeOffset offsetUtc = DateTimeOffset.FromUnixTimeSeconds(dateUnixTimeSeconds);
			DateTimeOffset offsetLocal = offsetUtc.ToLocalTime();
			
			Console.WriteLine("Latitude: {0};\nLongitude: {1};\nTemp in celsius: {2};\nDate: {3}", 
				               lat, lon, KelvinToCelsius(kelvinDegrees).ToString("0,0", CultureInfo.InvariantCulture), 
				               offsetLocal);

			return 0;
		}
		
		private static async Task<int> PrintTempAndDateByCity(string city, string apiKey)
		{
			var builder = new UriBuilder("https://api.openweathermap.org/data/2.5/weather");
			var queryParameters = HttpUtility.ParseQueryString(builder.Query);
			
			queryParameters["q"] = city;
			queryParameters["appid"] = apiKey;
			builder.Query = queryParameters.ToString();
			Uri uri = builder.Uri;
			
			var request = new HttpRequestMessage(HttpMethod.Get, uri);
			var client = new HttpClient();
			
			HttpResponseMessage result = await client.SendAsync(request);
			result.EnsureSuccessStatusCode();
			
			string jsonContent = await result.Content.ReadAsStringAsync();
			JsonDocument jsonDocument = JsonDocument.Parse(jsonContent);
			
			double lat = jsonDocument.RootElement
				.GetProperty("coord")
				.GetProperty("lat")
				.GetDouble();
			
			double lon = jsonDocument.RootElement
				.GetProperty("coord")
				.GetProperty("lon")
				.GetDouble();

			await PrintTempAndDateByCoordinate(lat, lon, apiKey);
			
			return 0;
		}
		
		private static double KelvinToCelsius(double kelvinDegrees)
		{
			return kelvinDegrees - 273.15;
		}
	}
}